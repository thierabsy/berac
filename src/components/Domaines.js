import React from 'react';
import { Link } from 'react-router-dom';

import { domaines } from './data/domaines';
import { urlPath } from '../path';
import Intro from './shared/Intro';
import Download from './shared/Download';
import Rslinks from './shared/Rslinks';

let domainesItems = domaines.map(({domaine, icon, items}, i) => {
                              return (
                                  <div key={i} className="row position" >
                                    <div className="col col-md-4 col-sm-12 colLeft" >
                                      <div className="Domaines-left" >
                                        <img src={ `${ urlPath }/img/icons/${icon}.png` } alt="" />
                                        <h1>{ domaine }</h1>
                                      </div>
                                    </div>
                                    <div className="col col-md-8 col-sm-12" >
                                      <div className="Domaines-right" >
                                        <h1>{ domaine }</h1>
                                        {
                                          items &&
                                          <ul>
                                            { items &&
                                              items.map((item, idx) => {
                                                    return <li key={idx} > { item } </li>
                                                  })
                                            }
                                          </ul>
                                        }
                                      </div>
                                    </div>
                                  </div>
                              )
                            })


                           

const Domaines = (props) => {
  window.scrollTo(0, 0);
  return (
    <div>
      <Intro path={ props.match.path } />
      <div className="Domaines">
        <div className="container">
          <div className="row">
            <div className="col col-md-9 col-sm-12 Content">
              { domainesItems }
            </div>
            <div className="col col-md-3 col-sm-12">
              <Download />
              <hr />
              <div className="ftH1"><h1>Réseaux Sociaux</h1></div>
              <Rslinks />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Domaines;