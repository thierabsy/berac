import React, { Component } from 'react';
import Topboxes from './accueil/Topboxes';
import About from './accueil/About';
import ExpDom from './accueil/ExpDom';
import Partenaires from './accueil/Partenaires';
import Separator from './shared/Separator';

class Accueil extends Component {
  componentDidMount() {
  window.scrollTo(0, 0)
}
// pytop(){}
  render() {
    return (
      <div className="Accueil">
          <Topboxes />
          <About />
          <Separator />
          <ExpDom />
          <Partenaires />
      </div>
    );
  }
}

export default Accueil;
