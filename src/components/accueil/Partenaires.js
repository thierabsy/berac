import React from 'react';

import { partenaires } from '../data/partenaires';
import { urlPath } from '../../path';

let partenairesItems = partenaires.map(({nom, sigle, logo, website}, i) => {
                              return(
                                <div key={i} className="expert" > 
                                  <img src={ `${ urlPath }/img/logo/${logo}` } alt={sigle} />
                                  <div className="expertText">
                                    <a href={website} target="_blank">
                                      <h4> { nom } </h4>
                                      <p> { sigle }</p>
                                    </a>
                                  </div>
                                </div>
                              )
                            }); 

const Partenaires = () => {
  return (
    <div className="Experts">
      <div className="Experts-overlay">
        <div className="container">
          <h3>Nos Partenaires</h3>
          <div className="expertsWrapper">
            { partenairesItems }
          </div>
        </div>
      </div>
    </div>
  );
}

export default Partenaires;
