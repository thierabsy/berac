import React from 'react';
import { NavLink } from 'react-router-dom';

import { topboxes } from '../data/topboxes';
import { urlPath } from '../../path';

let topboxesItems = topboxes.map(({title, text, icon}, index) => {
  return(
    <div key={index} className="col col-md-4 col-sm-12">
      <div className="singleBox">
        <img src={ `${urlPath}/img/icons/${icon}.png` }  alt="" />
        <h3>{ title }</h3>
        <p>{ text }</p>
      </div>
    </div>
  )
});

const Topboxes = () => {
  return (
      <div className="container">
        <div className="topboxes">
          <div className="row">
            { topboxesItems }
          </div>
        </div>
      </div>
  );
}

export default Topboxes;
