import React from 'react';
import { Link } from 'react-router-dom';

import { expertises } from '../data/expertises';
import { domaines } from '../data/domaines';
import { urlPath } from '../../path';

let expertisesItems = expertises.slice(0, 6)
                            .map((expertise, i) => {
                              return <li key={i} ><i className="fa fa-chevron-circle-right" /> { expertise } </li>
                            });
 
let domainesItems = domaines.map(({domaine, icon, items}, i) => {
                              return (
                                <div className="domaine" key={i}>
                                  <img src={ `${ urlPath }/img/icons/${icon}.png` } alt="" />
                                  <h1>{ domaine }</h1>
                                  {
                                    items &&
                                    <ul>
                                      { items &&
                                        items.slice(0, 2)
                                            .map((item, idx) => {
                                              return <li key={idx} ><i className="fa fa-chevron-circle-right" /> { item } </li>
                                            })
                                      }
                                    </ul>
                                  } 
                                </div>
                              )
                            })



const ExpDom = () => {
  return (
    <div className="ExpDom">
      <div className="ExpDom-overlay">
      <div className="container">
        <div className="row" >
          <div className="col col-md-4 col-sm-12 ExpDom-left-col" >
            <div className="ExpDom-left" >
              <h3>Expertises</h3>
              <div className="expertisesWrapper">
                <ul>
                  { expertisesItems }
                </ul>
                <div className="enSavoirPlus"> <Link to="/expertise">En savoir plus <i className="fa fa-angle-double-right" /> </Link></div> 
              </div>
            </div>
          </div>
          <div className="col col-md-8 col-sm-12" >
            <div className="ExpDom-right" >
              <h3>Domaines d'Intervention</h3>
              <div className="domainesWrapper">
                { domainesItems }
              </div>
              <div className="enSavoirPlus"> <Link to="/domaines">En savoir plus <i className="fa fa-angle-double-right" /> </Link> </div> 
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  );
}

export default ExpDom;
