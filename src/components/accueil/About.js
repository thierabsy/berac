import React from 'react';
import { Link } from 'react-router-dom';

const About = () => {
  return (
    <div className="About">
      <div className="container">
        <div className="row" >
          <div className="col col-lg-4 col-md-12 col-sm-12 about-left" >
            <h1>A<br/> Propos de<br/> Berac</h1>
          </div>
          <div className="col col-lg-8 col-md-12 col-sm-12 about-right" >
            <div className="inner-div">
              <h2>Berac</h2>
              <h4>Bureau d’Etudes, de Réalisations, d’Assistance et Conseils </h4>
              <p><strong>BERAC</strong>  est  un Cabinet qui travaille sur les questions du développement économique et social au Sénégal et en Afrique.</p>
              <p>Le Cabinet BERAC offre des services de consultance aux entreprises, Etats, ONG,  Associations, Fondations, etc., afin de les accompagner à conforter et à consolider leurs stratégies de développement socio-économique et à améliorer leur visibilité, leur rentabilité et leur croissance.</p>
              <div className="enSavoirPlus"> <Link to="/presentation">En savoir plus <i className="fa fa-angle-double-right" /> </Link> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default About;
