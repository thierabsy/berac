import React, {Component} from 'react';
import axios from 'axios';

import { contact } from "./data/contact";
import { urlPath } from '../path';
import Intro from './shared/Intro';
import Rslinks from './shared/Rslinks';
import Download from './shared/Download';

class Contact extends Component {
  constructor(props){
    super(props)

    this.state = {
      data: {
        nom: "",
        email: "",
        telephone: "",
        message: ""
      },
      envoi: "",
      status: false
    }
    this.fieldChange = this.fieldChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }
  fieldChange(e){
    this.setState({
        data: { 
            ...this.state.data,
            [e.target.name]: e.target.value
        }
    })
    // localStorage.setItem([e.target.name], e.target.value);
  }
  submitForm(e){
    e.preventDefault();
    let data = new FormData();
    for(let key in this.state.data){
        data.append(key , this.state.data[key]);
    }
    let url= urlPath+"/mailer";
    axios.post(url, data)
        .then(() => this.setState({
                                envoi: "Votre message a été envoyé avec success!", 
                                status: "oui",
                                data: {}
                              })
                            )
        .catch((e) => this.setState({envoi: "L'envoi de votre message a échoué!", status: "non"}))
  }
  render(){
    return (
      <div>
        <Intro path={ this.props.match.path } /> 
        <div className="Contact">
          <div className="container">
            <div className="row">
            <div className="col col-md-9 col-sm-12 Content">
            <div className="coordonnees">
              <div className="row"> 
                <div className="col col-sm-12 col-md-6">
                  <div className="innerWrapper iw-left">
                    <div className="iw-header"><h4>Coordonnées</h4></div>
                    <p><span>Adresse:</span> { contact.adresse } </p>
                    <p><span>BP:</span> { contact.bp } </p>
                    <p><span>Email:</span><a href= { `mailto:${ contact.email } ` } >{ contact.email } </a>  </p>
                    <p><span>Téléphone:</span>(fixe)<a href={ `tel:${ contact.telephoneFixe }` } > { contact.telephoneFixe }</a>  </p>
                    <p>                       (mob.)<a href={ `tel:${ contact.telephoneMobile }` } > { contact.telephoneMobile } </a> </p>
                  </div>
                </div>
                <div className="col col-sm-12 col-md-6">
                  <div className="innerWrapper iw-right">
                    <div className="iw-header"><h4>Horaires</h4></div>
                    <p><span>Jours ouvrables:</span> 09h-18h </p>
                    <p><span>Samedi:</span> 09h-15h </p>
                    <p><span>Dimanche:</span> ***** </p>
                    <br />
                    <hr />
                     <div className="note"> <span>Ces horaires sont succeptibles de changer en fonction des urgences.</span> </div>
                    {/* <p style={{color: 'transparent'}} >.</p> */}
                    
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <h3>Nous écrire:</h3>
            <form onSubmit={(e) => this.submitForm(e)}> 
              <div className="formField name" >
                <label>Nom et Prénoms <span>*</span> </label>
                <input type="text" className="form-control" name="nom" defaultValue={ this.state.data.nom } onChange={(e) => this.fieldChange(e)} placeholder="Nom-Prenoms" required />
              </div>
              <div className="row">
                <div className="col col-md-7 col-sm-12">
                  <div className="formField name" >
                    <label>Email <span>*</span> </label>
                    <input type="email" className="form-control" name="email" defaultValue={ this.state.data.email }  onChange={(e) => this.fieldChange(e)} placeholder="contact@beraconsulting.com" required />
                  </div>
                </div>
                <div className="col col-md-5 col-sm-12">
                  <div className="formField name" >
                    <label>Téléphone</label>
                    <input type="telephone" className="form-control" name="telephone" defaultValue={ this.state.data.telephone }  onChange={(e) => this.fieldChange(e)} placeholder="+221 000 00 00" />
                  </div>
                </div>
              </div>
              <div className="formField name" >
                <label>Message <span>*</span></label>
                <textarea rows="10" className="form-control" name="message" defaultValue={ this.state.data.message }  onChange={(e) => this.fieldChange(e)} placeholder="Votre message..." required></textarea>
              </div>
              <div className="submitBtn"><button className="btn btn-large" type="submit">Envoyer</button></div>
            </form>
            <br />
            { this.state.envoi !== "" && 
              <div className={`alert ${ this.state.status === "oui" ? "alert-success" : "alert-danger"}`} > 
                <i className={`fa ${ this.state.status === "oui" ? "fa-check" : "fa-exclamation-triangle"}`} /> { this.state.envoi } 
              </div>
            }
            </div>
            <div className="col col-md-3 col-sm-12">
              <Download />
              <hr />
              <div className="ftH1"><h1>Via Réseaux Sociaux</h1></div>
              <Rslinks />
            </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Contact;
