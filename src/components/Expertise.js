import React, {Component} from 'react';

import { expertises } from './data/expertises';
import { experts2 } from './data/experts2';
import Intro from './shared/Intro';
import Download from './shared/Download';
import Rslinks from './shared/Rslinks';

class Expertise extends Component {
  constructor(props){
    super(props);
    this.state = {
      min: 0,
      max: 6
    }
    this.suivant =this.suivant.bind(this);
    this.precedent =this.precedent.bind(this);
    this.btns =this.btns.bind(this);
  }
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  suivant(){
    let incr =  ((this.state.max + 6) >= expertises.length) ? (expertises.length - this.state.max) : 6 ;
    this.setState({
      min: this.state.min + incr,
      max: this.state.max + incr,
    })
  }
  precedent(){
    let decr =  ((this.state.min - 6) >= 0) ?  6 : (this.state.min - 0);
    this.setState({
      min: this.state.min - decr,
      max: this.state.max - decr
    })
  }
  btns(){
    return(
      <div className="btns">
        {/* { (this.state.min  > 0) && <button className="btn btn-default btn-sm"  onClick={()=> this.precedent()} ><i className="fa fa-caret-left" /> Précédent</button>}  */}
        <button className="btn btn-default btn-sm" disabled={(this.state.min  > 0) ? false : true }  onClick={()=> this.precedent()} ><i className="fa fa-caret-left" /> Précédent</button>
        {/* { (this.state.max < expertises.length ) && <button className="btn btn-default btn-sm" onClick={()=> this.suivant()} >Suivant  <i className="fa fa-caret-right" /></button> } */}
        <button className="btn btn-default btn-sm" disabled={ (this.state.max < expertises.length ) ? false : true } onClick={()=> this.suivant()} >Suivant  <i className="fa fa-caret-right" /></button>
       </div>
    )
  }

  render(){
    
    let expertisesItems = expertises.slice(this.state.min, this.state.max).map((expertise, i) => {
      return <div className="col col-md-4 col-sm-12" key={i}> <div className="expertise">  { expertise } </div> </div>
    });

    let expertsItems = experts2.map(({nom, specialite}, i) => {
      return <li className="expert" key={i}> <span className="span1">{ nom }</span>, <span className="span2"> { specialite } </span> </li>
    });

    return (
      <div>
        <Intro path={ this.props.match.path } />
      <div className="Expertise">
        <div className="container">
          <div className="row">
            <div className="col col-md-9 col-sm-12 Content">
              <div className="expertise-wrapper">
                <div className="expertise-header">
                  <h4>Notre Expertise</h4>
                  <p>Les prestations fournies par le Cabinet sont assurées par un groupe d’Experts compétents titulaires pour la plupart de Doctorat et de Master qui ont capitalisé une expérience avérée dans les différents domaines d’expertise mentionnés notamment en Management de projets, programmes et portefeuilles, tant dans le domaine économique que social.</p>
                  <p>L’équipe de BERAC compte également un pool d’experts spécialisés dans l’ingénierie financière et la conduite de missions financières.</p>
                </div>
                <hr />
                { this.btns() }
                <div className="expertise-grid">
                   <div className="row">
                      { expertisesItems }
                   </div>
                </div> 
                { this.btns() }
                <hr />
               </div>
                <h4>Nos Experts Associés</h4>
                <p>Les experts associés du Cabinet BERAC sont reconnus au plan mondial, sont polyvalents et multilingues. Ils  capitalisent plusieurs années d’intervention sur le terrain aussi bien dans de grands projets d’infrastructures que dans les secteurs de la protection sociale, de la sécurité humaine, des droits humains, de la formation professionnelle, de l’apprentissage, des finances, de la recherche et de la formation des adultes.</p>
                <p>Parmi eux, on peut citer :</p>
               <div className="experts-grid">
                <ul>
                  { expertsItems }
                </ul>
               </div>
            </div>
            <div className="col col-md-3 col-sm-12">
              <Download />
              <hr />
              <div className="ftH1"><h1>Réseaux Sociaux</h1></div>
              <Rslinks />
            </div>
          </div>
        </div>
      </div>
      </div> 
    );
  }
}

export default Expertise;