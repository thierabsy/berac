import React from 'react';
import { urlPath } from '../../path';

const Berac = () => {
  return (
    <div className="Berac">
      <img src={ `${ urlPath }/img/business3.png` } className="img-top" alt="A Propos de Berac" />
      <h3>Berac</h3>
      <h4>Bureau d’Etudes, de Réalisations, d’Assistance et Conseils</h4>
      <hr />
      <p>Est  un Cabinet qui travaille sur les questions du développement économique et social au Sénégal et dans la sous-région.</p>
      <p>Le Cabinet BERAC offre des services de consultance aux entreprises, Etats, ONG, Associations, Fondations, etc., afin de les accompagner à conforter et à consolider leurs stratégies de développement socio-économique et à améliorer leur visibilité, leur rentabilité et leur croissance.
          BERAC capitalise déjà une solide expérience et une forte expertise en management stratégique de projets, programmes et portefeuilles et a le privilège de s’appuyer sur des références mondiales en la matière ainsi que dans le domaine des finances.
      </p>
      <div className="row">
        <div className="col col-sm-12 col-md-6 iconCol">
          <img src={ `${ urlPath }/img/icons/projection.png` } className="img-icon" alt="Berac Methodes" />
        </div>
        <div className="col col-sm-12 col-md-6">
          <p>BERAC combine séminaires et ateliers professionnels qui prennent en compte les problématiques de croissance sociale et économique dans une optique de consolidation et de pérennisation des acquis pouvant avoir un impact réel sur le développement. </p> 
        </div>
      </div>
      <div className="">
        <p>Le Bureau d’Etudes, de Réalisations, d’Assistance et Conseils a une approche dynamique et flexible qui consiste à accompagner les entre¬prises, les Etats, les ONG, les Associations, les Fondations, et autres institutions dans leurs politiques et stratégies de développement dans la perspective de renforcer leur efficacité et leur performance par le biais de méthodes managériales innovantes :</p>
        <ul>
          <li>Etudes de faisabilité;</li> 
          <li>Mise en œuvre ;</li>
          <li>Pilotage et conduite du changement ;</li>
          <li>Management de la qualité ;</li>
          <li>Exploitation, suivi et évaluation.</li>
        </ul>
      </div>
      <div className="row">
        <div className="col col-sm-12 col-md-6">
          <p>Le Cabinet de consultance BERAC compte fournir à ses clients une réelle valeur ajoutée par le biais de services innovants arrimés aux dernières méthodes managériales et à travers un vaste programme de séminaires sur mesure et de missions spécialisées et orientées entreprises, Etats ONG, Associations, etc., bien conçus pour répondre aux exigences du développement.</p>
        </div>
        <div className="col col-sm-12 col-md-6 iconCol">
          <img src={ `${ urlPath }/img/icons/approche-cc.png` } className="img-icon" alt="Berac Approche" />
        </div>
      </div>
    </div>
  );
}

export default Berac;
