import React from 'react';
import Berac from './Berac';
import Mvv from './Mvv';

const Content = ({page}) => {
  let mvv = "mission-vision-valeurs";
  return (
    <div className="Content">
      {
        page !== mvv ?
        <Berac /> :
        <Mvv />
      }
    </div>
  );
}

export default Content;
