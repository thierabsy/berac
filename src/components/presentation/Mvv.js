import React from 'react';
import { urlPath } from '../../path';

const Mvv = () => {
  return (
    <div className="Mvv">
      <div className="line mission">
        <div className="col col-md-4 col-sm-12 title">
          <img src={ `${ urlPath }/img/icons/mission.png` } className="img-icon" alt="Berac Methodes" />
        </div> 
        <div className="col col-md-8 col-sm-12 text">
          <h1>MISSION</h1> 
          <p>La mission du Bureau d’Etudes, de Réalisations, d’Assistance et Conseils (BERAC)  est d’accompagner et d’appuyer les ONG, Associations, Fondations, Entreprises, Etats, Institutions, etc., dans la formation, le management et l’exécution de leurs politiques et stratégies de développement.</p>
          <p>BERAC vise à contribuer à la promotion et à l’application des meilleures pratiques de développement notamment dans le domaine du management des projets, programmes et portefeuilles en Afrique, en tant que leviers de l’émergence des Etats et Organisations.</p>
        </div> 
      </div>
      <div className="line vision">
        <div className="col col-md-4 col-sm-12 title">
          <img src={ `${ urlPath }/img/icons/vision.png` } className="img-icon" alt="Berac Vision" />
        </div> 
        <div className="col col-md-8 col-sm-12 text">
          <h1>VISION</h1>
          <p>BERAC vise à devenir dans un avenir proche un des leaders en matière de consultance et d’assistance aux Entreprises, 
            Etats, Institutions, ONG et Associations, etc., en management de projets, programmes et portefeuilles. 
          </p>
        </div>
      </div>
      <div className="line valeurs">
        <div className="col col-md-4 col-sm-12 title">
          <img src={ `${ urlPath }/img/icons/valeurs.png` } className="img-icon" alt="Berac Valeurs" />
        </div> 
        <div className="col col-md-8 col-sm-12 text">
          <h1>Valeurs</h1>
          <p>Les valeurs du Cabinet BERAC se traduisent par : </p>
          <div className="valeurs-items">
            <span><i className="fa fa-check" /> la responsabilité</span>
            <span><i className="fa fa-check" /> l’éthique et l’intégrité</span>
            <span><i className="fa fa-check" /> la confidentialité</span>
            <span><i className="fa fa-check" /> la qualité de service</span>
            <span><i className="fa fa-check" /> le respect des délais</span>
          </div>
        </div> 
      </div> 
      
    </div>
  );
}

export default Mvv;
