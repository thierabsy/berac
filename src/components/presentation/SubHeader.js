import React from 'react';
import { Link } from 'react-router-dom';
 
const SubHeader = ({page}) => {
  let mvv = "mission-vision-valeurs";
  return (
    <div className="SubHeader">
      <ul>
        <li className={ page === "berac" || page === undefined  ? "subheader-active" : undefined }><Link to="?page=berac">Berac</Link></li>
        <li className={ page === mvv ? "subheader-active" : undefined }><Link to={`?page=${mvv}`} >Mission, Vision, Valeurs</Link></li>
      </ul>
    </div>
  );
}

export default SubHeader;
