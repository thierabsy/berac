import React from 'react';
import FooterTop from './FooterTop';
import FooterText from './FooterText';
import FooterCopy from './FooterCopy';

const Footer = (props) => {
  return (
    <div className="Footer">
      <FooterTop />
      <FooterText />
      <FooterCopy />
    </div>
  );
}

export default Footer;
