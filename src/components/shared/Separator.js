import React from 'react';
import { urlPath } from '../../path';

const Separator = () => {
  return (
    <div className="Separator">
      <div className="container">
        <div className="row">
          <div className="col-md-4 col-sm-12">
          </div>
          <div className="col-md-8 col-sm-12 separator-col">
            <img src={ `${ urlPath }/img/icons/handshake.png` } alt="" />
            <span>Plus qu'un client, nous sommes votre partenaire...</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Separator;
