import React from 'react';
import { urlPath } from '../../path';

// let year = new Date().getFullYear();

const Intro = ({path}) => {
  let titre = path.replace('/', '');
  return (
    <div className="Intro">
      <div className="intro-img">
        <img src={ `${ urlPath }/img/icons/maze.png` } alt="" /> <span>{titre}</span>
      </div>
    </div>
  );
}

export default Intro;
