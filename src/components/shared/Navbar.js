import React from 'react';
import { NavLink } from 'react-router-dom';

import { navbarData } from '../data/navbar';

const NavItems = ({menu}) => navbarData.map(({name, link, subMenu}, index) => {
  return(
    <li key={index} className="menu-item">
      <NavLink exact={true} to={ `/${link}` } activeClassName='menu-item-active' onClick= {() => menu()} > { name } </NavLink>
        {
          subMenu &&
          <ul className="sub-menu">
              {
                subMenu.map(({name, link}, i) => {
                  return(
                    <li key={i} className="sub-menu-item">
                      <NavLink to={ link } > { name } </NavLink>
                    </li>
                  )
                })
              }
          </ul>
        }
    </li> 
  )
});

const Navbar = ({menu, showMenu}) => {
  return (
      <div>
        <div className="navbar">
          <ul className="menu">
             <NavItems menu = {() => ""} /> 
            <li className="responsiveMenu"  onClick={()=> menu()}><i className="fa fa-bars" /></li>
          </ul>
        </div>
        {
          showMenu &&
          <div className="responsiveMenuItems">
            <div className="close" onClick={()=> menu()} >Fermer </div>
            
            <ul>
              <NavItems menu = { menu } /> 
            </ul>
          </div>
        }
      </div>
  );
}

export default Navbar;
