import React from 'react';


const Rslinks = () => {
  return (
    <div className="rs-links">
      <div className="rs-link"> <a href="#"> <i className="fa fa-facebook" /> <span>Facebook</span> </a></div>
      <div className="rs-link"> <a href="#"> <i className="fa fa-linkedin" /> <span> Linkedin </span> </a></div>
      <div className="rs-link"> <a href="#"> <i className="fa fa-skype" /> <span>Skype</span> </a></div>
      <div className="rs-link"> <a href="#"> <i className="fa fa-whatsapp" /> <span> Whatsapp </span> </a></div>
    </div>
  );
}

export default Rslinks;
