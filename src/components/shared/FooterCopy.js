import React from 'react';

let year = new Date().getFullYear();

const FooterCopy = () => {
  return (
    <div className="FooterCopy">
      &copy; {year } Copyright Berac, site crédit <a href="https://www.linkedin.com/in/thierno-ablaye-sy">Sytekr</a>
    </div>
  );
}

export default FooterCopy;
