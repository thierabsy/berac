import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Navbar from './Navbar';
import TopContact from './TopContact';
import { urlPath } from '../../path';

class Header extends Component {
  constructor(props){
    super(props);
    this.state= {
      showMenu: false
    }
    
    this.menu = this.menu.bind(this);
  }

  menu(){
    this.setState({
      showMenu: !this.state.showMenu
    })
  }

  render() {
    
    let headerHeight = window.location.href === urlPath+"/" ? "75vh" : "50vh";

    return (
      <div className="Header" style={{minHeight: headerHeight}} >
        <div className="header-overlay">
          <div className="container">
            <TopContact />
            <div className="navbarwrapper">
              <Navbar showMenu={this.state.showMenu} menu={this.menu} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
