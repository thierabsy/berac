import React from 'react';
import { urlPath } from '../../path';

const FooterTop = () => {
  let notAcccueil = window.location.href !== urlPath+"/";
  return (
    <div className="FooterTop">
      { notAcccueil && <div className="ftopBar" />}
      <div className="container">
        <div className="row">
          <div className="col col-md-4 col-sm-12 ">
            <div className="ftWrapper">
              <div className="ftIcon">
                <img src={ `${ urlPath }/img/icons/experts.png` } alt="" />
              </div>
              <div className="ftText">
                <span>nous collaborons avec</span>
                <p><strong>+30</strong> Experts et partenaires</p>
              </div>
            </div>
          </div>
          <div className="col col-md-4 col-sm-12">
           <div className="certificate">
            <img src={ `${ urlPath }/img/icons/certificate.png` } className="certificate-img" alt="" />
           </div>
          </div>
          <div className="col col-md-4 col-sm-12">
            <div className="ftWrapper">
              <div className="ftIcon">
                <img src={ `${ urlPath }/img/icons/industry.png` } alt="" />
              </div>
              <div className="ftText">
                <span>nous intervenons dans</span>
                <p><strong>+5</strong> Domaines</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FooterTop;
