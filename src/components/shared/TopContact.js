import React from 'react';
import { NavLink } from 'react-router-dom';

import { urlPath } from '../../path';
import { contact } from '../data/contact';

const TopContact = () => {
  return (
        <div className="topcontact">
        <div className="top-logo">
          <img src={`${ urlPath }/img/logo.jpg`} className="logo" alt="berac_logo" />
        </div> 
        <div className="top-contact">
          <ul>
            <li>
              <div className="contact-img">
                <img src={`${ urlPath }/img/icons/telephone.png`} className="contact" alt="" />
              </div>
              <div className="contact-text">
                <p className="contact-title">Téléphone:</p>
                <p className="contact-content"><a href={ `tel:${contact.telephoneFixe}` }> { contact.telephoneFixe } </a></p>
                <p className="contact-content"><a href={ `tel:${contact.telephoneMobile}` }> { contact.telephoneMobile }</a> </p>
              </div>
            </li>
            <li>
              <div className="contact-img">
                <img src={`${ urlPath }/img/icons/email.png`} className="contact"  alt=""/>
              </div>
              <div className="contact-text">
                <p className="contact-title">Email:</p>
                <p className="contact-content"><a href={`mailto:${contact.email}`}> { contact.email } </a></p>
                <p className="contact-content">.</p>
               </div>
            </li>
          </ul>
        </div>
        </div>
  );
}

export default TopContact;
