import React from 'react';
import { urlPath } from '../../path';
import { contact } from '../data/contact';
import Rslinks from './Rslinks';

const FooterText = () => {
  return (
    <div className="FooterText">
      <div className="container">
        <div className="row">
          <div className="col col-md-4 col-sm-12">
            <div className="ftText-1">
              <img src={ `${ urlPath }/img/logo.jpg` } alt="" />
              <h1>Bureau d’Etudes, de Réalisations, d’Assistance et Conseils</h1>
              <p>***** <br /> <strong>Offre des services de consultance</strong> aux entreprises, Etats, ONG, Associations, Fondations, etc</p>
            </div>
          </div>
          <div className="col col-md-5 col-sm-12">
            <div className="ftText-2 ftH1">
              <h1>Nous Contacter</h1>
              <p><i className="fa fa-home" /><span> {contact.adresse } </span></p>
              <p><i className="fa fa-at" /><span> {contact.email } </span></p>
              <p><i className="fa fa-phone" /><span> {contact.telephoneFixe } </span></p>
              <p><i className="fa fa-mobile" /><span> {contact.telephoneMobile } </span></p>
            </div>
          </div>
          <div className="col col-md-3 col-sm-12">
            <div className="ftText-3 ftH1">
              <h1>.....Réseaux Sociaux</h1>
              <Rslinks />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FooterText;
