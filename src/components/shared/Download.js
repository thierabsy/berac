import React from 'react';
import { urlPath } from '../../path';

let q = document.getElementById("download")
// console.log(q.offsetTop);
// getOffset(){
//   setInterval(()=> {console.log(q.offsetTop)}, 100)
// }
const Download = () => {
  return (
    <div className="Download" id="download" >
      <a href={`${urlPath}/berac.pdf`} target="_blank">
        <div className="Download-inner">
          <span> Télécharger <br /> notre <br /> brochure</span>
        </div>
      </a>
    </div>
  );
}

export default Download;
