export const topboxes = [
    {
        title: "Clé Success 1",
        text: "Un groupe d’experts professionnels avec des compétences variées et complémentaires.",
        icon: "but"
    },
    {
        title: "Clé Success 2",
        text: "Une solide expérience dans des secteurs stratégiques et innovants.",
        icon: "objectif"
    },
    {
        title: "Clé Success 3",
        text: "Une dynamique d’équipe basée sur une approche participative.",
        icon: "approche"
    }
]
 
