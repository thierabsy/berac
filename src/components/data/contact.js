export const contact = {
    adresse: "Ouest Foire, Cité Télécom, Villa N° 28",
    email: "contact@beraconsulting.com",
    telephoneFixe: "+221 33 864 29 64",
    telephoneMobile: "+221 77 547 79 66",
    bp: "64644 Dakar-SENEGAL",
}