export const partenaires = [
    {
        nom: "CASR3PM",
        sigle: "Centre en Management de Projets, Programmes ...",
        logo: "logo_casr3pm.png",
        website: "http://casr3pm.sn",
    },
    {
        nom: "NMS Africa",
        sigle: "NMS Africa Consulting",
        logo: "logo_nms.png",
        website: "http://nms-africa.org",
    },
    {
        nom: "Fondation Paul Gérin Lajoie",
        sigle: "Fondation Paul Gérin Lajoie",
        logo: "logo_fondation_paul_gerin_lajoie.jpg",
        website: "https://fondationpgl.ca/accueil/",
    },
    {
        nom: "2PMA",
        sigle: "PanAfrican Project Management Association",
        logo: "logo_2pma.jpg",
        website: "https://www.google.sn/search?q=2pma",
    },
    {
        nom: "BIT",
        sigle: "Bureau International du Travail",
        logo: "logo_oit.png",
        website: "http://www.ilo.org/global/lang--fr/index.htm",
    },
    {
        nom: "ENDA GRAF",
        sigle: "ENDA GRAF Sahel et Afrique de l’Ouest ",
        logo: "logo_enda_graf_sahel.jpg",
        website: "http://www.endagrafsahel.org/Realisation-de-Enda-Graf-Sahel",
    },
    
]


