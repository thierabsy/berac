export const domaines = [
    {
        domaine: "GESTION DE PROJETS PROGRAMMES ET PORTEFEUILLES",
        icon: "domaine-projet",
        items: [
            "Gestion axée sur les résultats",
            "Suivi et évaluation de projets, programmes et portefeuilles",
            "Gestion opérationnelle",
            "Gestion des risques de projets",
            "Etudes de préfaisabilité et de faisabilité",
            "Partenariat public-privé"
        ]
    },
    {
        domaine: "GESTION STRATEGIQUE DES ENTREPRISES ",
        icon: "domaine-strategique",
        items: [
            "Gestion des ressources humaines(GRH)", 
            "Gestion prévisionnelle des emplois et des compétences(GPEC)",
            "Leadership et entrepreneurship",
            "Communication professionnelle",
            "Ingénierie financière (bilans, comptes de résultats, réalisation de tableaux de bord, ratios)",
            "Privatisation et restructuration",
            "Fiscalité et Droit des affaires",
            "Rapports de gestion/Business-plan",
            "Mise en place de systèmes de contrôle de gestion",
            "Management de la qualité"

        ]
    },
    {
        domaine: "DECENTRALISATION ET DEVELOPPEMENT LOCAL",
        icon: "domaine-decentralisation",
        items: [
            "Décentralisation, développement local et ingénierie sociale",
            "Appui-conseils en stratégie aux ONG et Associations",
            "Droits humains et bonne gouvernance locale",
            "Diagnostic organisationnel",
        ]
    },
    {
        domaine: "ETUDES",
        icon: "domaine-etude",
        items: [
            "Etudes de marché", 
            "Etudes diagnostic",
            "Etudes et recherches socio-économiques qualitatives et quantitatives",
            "Evaluation",
        ]
    },
    {
        domaine: "APPUI AUX SECTEURS INFORMEL/FORMEL",
        icon: "domaine-secteur",
        items: [
            "Apprentissage rénové",
            "Formation professionnelle",
            "Renforcement de capacités"
        ]
    },
]


