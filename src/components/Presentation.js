import React, {Component} from 'react';
import queryString from 'query-string/index';
import SubHeader from './presentation/SubHeader';
import Content from './presentation/Content';
import Intro from './shared/Intro';
import Download from './shared/Download';
import Rslinks from './shared/Rslinks';

class Presentation extends Component {
  render(){

    let queries = queryString.parse(this.props.location.search);
    let page = queries.page;
    return (
      <div>
        <Intro path={ this.props.match.path } />
        <div className="Presentation">
        <div className="container">
              <SubHeader page={ page } />
          <div className="row">
            <div className="col col-md-9 col-sm-12">
              {/* <hr /> */}
              <Content page={ page } />
            </div>
            <div className="col col-md-3 col-sm-12">
              <Download />
              <hr />
              <div className="ftH1"><h1>Réseaux Sociaux</h1></div>
              <Rslinks />
            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default Presentation;
