import React from 'react';
import {BrowserRouter, Route } from 'react-router-dom';

import Header from './components/shared/Header';
import Accueil from './components/Accueil';
import Presentation from './components/Presentation';
import Expertise from './components/Expertise';
import Domaines from './components/Domaines';
import Contact from './components/Contact';
import Footer from './components/shared/Footer';


function scrollToTop(scrolled) {
  if(scrolled >= 400){
    document.getElementById("scrollTop").className= "scrollTop show";
  }else{
    document.getElementById("scrollTop").classList.remove("show");
  }
}

window.addEventListener('scroll', function(e) {
  var position = window.scrollY;
  window.requestAnimationFrame(function() {
    scrollToTop(position);
  });
});

let scrollTop1 = () => {
  let int = null;
  int = setInterval(()=> {
    if(window.pageYOffset <= 0 ){
      clearInterval(int);
      return;
    };
    window.scrollTo(0, window.pageYOffset-(Math.random()*100))}, 
    20
  );
}

const Myroutes = () => {
    return (
      <div>
        <BrowserRouter >
          <div>
            <div id="scrollTop" className="scrollTop" onClick={()=> scrollTop1()} ><i className="fa fa-angle-double-up" /> </div>
            <Header />
            <Route exact = { true } path="/" component={ Accueil } />
            <Route path="/presentation" component={ Presentation } />
            <Route path="/expertise" component={ Expertise } />
            <Route path="/domaines" component={ Domaines } />
            <Route path="/contact" component={ Contact } />
            <Footer />
          </div>
        </BrowserRouter>
      </div>
    );
}

export default Myroutes;


